from django.urls import path
from django.views.i18n import JavaScriptCatalog

from .views import Exmaple

urlpatterns = [
    path("", Exmaple.as_view(), name="index"),
    path("catalog/", JavaScriptCatalog.as_view(), name="javascript-catalog"),
]
