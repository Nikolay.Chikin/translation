from django.utils.translation import gettext
from django.views.generic import TemplateView


class Exmaple(TemplateView):
    template_name = "example.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["translation"] = gettext("Translation context example")
        return context
